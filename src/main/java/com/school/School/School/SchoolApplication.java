package com.school.School.School;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.school.School.School.Model.AuditorAwareImpl;


@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@SpringBootApplication
public class SchoolApplication {
	
	 @Bean
	 public AuditorAware<String> auditorAware() {
	        return new AuditorAwareImpl();
	 }

	public static void main(String[] args) {
		SpringApplication.run(SchoolApplication.class, args);
	}
	
}
