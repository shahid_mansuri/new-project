package com.school.School.School.Repo;

import java.util.List;
import java.util.stream.Stream;

import com.school.School.School.Model.Student;



public interface StudentRepo {
	
	void addStudent(Student student);
	List<Student> getStudents();
	Student getStudentById(int id);
	List<Student> getStudentByDivision(String division);
	void deleteStudentById(int id);
	void updateStudentById(int id);
	void updateStudentByObj(Student student);
	
}
