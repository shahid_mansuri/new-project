package com.school.School.School.Repo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.school.School.School.Model.Student;
import com.school.School.School.ServiceImpl.StudentServiceImpl;

@Repository
public class StudentRepoImpl implements StudentRepo{
	
	Logger logger = LoggerFactory.getLogger(StudentRepoImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@Transactional
	public void addStudent(Student student) {
		
		entityManager.persist(student);
		
	}

	@Override
//	@Transactional
	public List<Student> getStudents() {
		
		Query query=entityManager.createNativeQuery("SELECT * FROM student",Student.class);
		List<Student> studentList = new ArrayList<Student>();
		studentList = query.getResultList();
	
		return studentList;
	}

	@Override
	@Transactional
	public Student getStudentById(int id) {
		
		return entityManager.find(Student.class,id);
	}

	@Override
	@Transactional
	public List<Student> getStudentByDivision(String division) {
		
		Query query=entityManager.createNativeQuery("SELECT * FROM student",Student.class);
		List<Student> studentList = new ArrayList<Student>();
		studentList = query.getResultList();
		
//		List<String> lids = new ArrayList<String>();
//		lids.add("A");
//		lids.add("B");
//		Query query = entityManager.createNativeQuery("SELECT * FROM student WHERE division = :d ", Student.class);
//		query.setParameter("d", division);
//		List<Student> studentList = new ArrayList<Student>();
//		studentList = query.getResultList();
//		
//		System.out.println(studentList);
		
		return studentList;
	}

	
	@Override
	@Transactional
	public void deleteStudentById(int id) {
		
		List<Integer> lids = new ArrayList<Integer>();
		lids.add(2);
		lids.add(4);
		
		//hard delete
//		Student stu = entityManager.find(Student.class, id);
		Query query = entityManager.createNativeQuery("DELETE FROM student WHERE id in :d", Student.class);
		query.setParameter("d", lids);
		query.executeUpdate();
		logger.info("Delete repo function called");
		
	}

	@Override
	@Transactional
	public void updateStudentById(int id) {
		
		int studentId = id;
		Student oldStudent = entityManager.find(Student.class,studentId);
		oldStudent.setFname("Aakash");
		entityManager.merge(oldStudent);
		
	}

	@Override
	@Transactional
	public void updateStudentByObj(Student student) {
		
		entityManager.merge(student);
		
		
	}
	
}
