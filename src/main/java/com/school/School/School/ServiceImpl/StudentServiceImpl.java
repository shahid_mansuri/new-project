package com.school.School.School.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.school.School.School.Model.Student;
import com.school.School.School.Repo.StudentRepo;
import com.school.School.School.Service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {
	
	Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Autowired
	private StudentRepo studentRepo;

	@Override
	public void addStudent(Student student) {

		studentRepo.addStudent(student);

	}

	@Override
	public List<Student> getStudents() {
		logger.info("GET ALL STUDENTS");
//		List<Student> getStudentStream =
		
//		List<Student> getStudentList = getStudentStream.stream().filter(p -> p.getId() != 3)
//				.collect(Collectors.toList());

//		Map<String, String> mapExp = new HashMap<String, String>();
//
//		Iterator<Student> it = getStudentList.iterator();
//		while (it.hasNext()) {
//
//			Student st = it.next();
//			mapExp.put(st.getFname(), st.getLname());
//
//		}
//		
//		for(Map.Entry<String, String> et: mapExp.entrySet()) {
//			
//			 System.out.println("Key = " + et.getKey() + 
//                     ", Value = " + et.getValue());
//		}

	
		
		return studentRepo.getStudents();
	}

	@Override
	public Student getStudentById(int id) {
		
		return studentRepo.getStudentById(id);
	}

	
	@Override
	public List<Student> getStudentByDivision(String division) {
		System.out.println("division---"+division);
		List<Student> getStudentList = studentRepo.getStudentByDivision(division);
		System.out.println("getStudentList---"+getStudentList);
		
		List<Student> finalList = getStudentList.stream().filter(p -> p.getDivision() .equals(division) )
				.collect(Collectors.toList());
		
		System.out.println("finalList---"+finalList);
		logger.info("Student By Division Method running"+finalList);
		return finalList;
		
	}

	@Override
	public void deleteStudentById(int id) {
	
		logger.info("Delete Student Method running"+id);
		studentRepo.deleteStudentById(id);
		
	}

	@Override
	public void updateStudentById(int id) {
		
		logger.info("Update Student By ID Method running");
		studentRepo.updateStudentById(id);
		
	}

	@Override
	public void updateStudentByObj(Student student) {
		
		logger.info("Update Student By Object Method running");
		studentRepo.updateStudentByObj(student);
		
	}

}
