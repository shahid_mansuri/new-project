package com.school.School.School.Model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity(name="student")
@EntityListeners(AuditingEntityListener.class)
public class Student extends Auditable<String>{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String fname;
	private String lname;
	private int roll_no;
	private String gender;
	private int standard;
	private String division;
		
	public Student() {
		
	}

	public Student(int id, String fname, String lname, int roll_no, String gender, int standard, String division) {
		super();
		this.id = id;
		this.fname = fname;
		this.lname = lname;
		this.roll_no = roll_no;
		this.gender = gender;
		this.standard = standard;
		this.division = division;
	}

	
}
