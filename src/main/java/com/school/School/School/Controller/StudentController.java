package com.school.School.School.Controller;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.school.School.School.Model.Student;
import com.school.School.School.Service.StudentService;


@RestController
public class StudentController {

	@Autowired
	private StudentService studentService;
	
	@GetMapping("/testUrl")
	public String testUrl() {
		
		return "Hi";
	}
	
	@PostMapping("/addStudent")
	public void addStudent(@RequestBody Student student) {
		
		studentService.addStudent(student);
		
	}
	
	//get all student
	@GetMapping("/getStudents")
	public List<Student> getStudents(){
		
		return studentService.getStudents();
	}
	
	//getStudentById
	
	@GetMapping("/getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int id) {
		
		return studentService.getStudentById(id);
	}
	
	//getStudentByDivision
	@GetMapping("/getStudentByDivision/{division}")
	public List<Student> getStudentByDivision(@PathVariable("division") String division){
		
		
		return studentService.getStudentByDivision(division);
	}

	//deleteStudentById
	@DeleteMapping("/deleteStudentById/{id}")
	public void deleteStudentById(@PathVariable("id") int id){
		
		studentService.deleteStudentById(id);
	}

	//updateStuentById
	@PutMapping("/updateStudentById/{id}")
	public void updateStudentById(@PathVariable("id") int id) {
		
		studentService.updateStudentById(id);
		
	}
	
	//updateStuentByObj
	@PutMapping("/updateStudentByObj")
	public void updateStudentByObj(@RequestBody Student student) {
		
		studentService.updateStudentByObj(student);
		
	}
	
}
